FROM node:12.15.0

RUN mkdir -p /srv/ensembl-help

COPY . /srv/ensembl-help

WORKDIR /srv/ensembl-help

RUN npm install

EXPOSE 1337

CMD [ "npm", "run", "develop" ]
